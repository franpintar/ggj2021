extends Control

onready var hp = $MarginContainer/HBox/Lifebar
onready var distance_label = $MarginContainer/HBox/MarginContainer/DistanceNumber
onready var tween = $Tween
var animated_health = 0

func _ready():
	var player_max_health = $"../Player".max_hp
	hp.max_value = player_max_health
	update_lifebar(player_max_health)
	var distance = $"../../World".distance_to_ship
	update_distance_label(distance)

func _process(_delta):
	hp.value = round(animated_health)

func update_lifebar(new_value):
	tween.interpolate_property(self, "animated_health", animated_health, new_value, 0.25)
	if not tween.is_active():
		tween.start()

func update_distance_label(new_value):
	distance_label.text = str(new_value)

func _on_Player_health_changed(player_health):
	update_lifebar(player_health)

func _on_World_distance_changed(new_distance):
	update_distance_label(new_distance)
