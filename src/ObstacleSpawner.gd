extends Node2D

var spawn_positions = null
var Asteroid = preload("res://characters/Asteroid.tscn")
var BrokenExhaust = preload("res://characters/BrokenExhaust.tscn")
var MetalDisk = preload("res://obstacles/MetalDisk.tscn")
var SolarPanel
var obstacles = [Asteroid, BrokenExhaust]

func _ready():
	randomize()

# TODO: Randomizar tiempo entre spawn de obstáculos

func spawn_obstacle():
	# Offset from (0, 0)
	var pos_offset = Vector2(randf() * (get_owner().get_screen_size().x - 128) + 64, 0) # TODO: Remove hard coded values
	# Index for type of obstacle
	var obstacle_index = randi() % obstacles.size()
	# Random boolean for determining if mirrored animation or not
	var is_mirrored = false
	if randi() % 2:
		is_mirrored = true
	
	var obstacle = obstacles[obstacle_index].instance()
	obstacle.set_global_position(pos_offset)
	add_child(obstacle)
	obstacle.get_node("AnimatedSprite").flip_h = is_mirrored

func _on_SpawnTimer_timeout():
	spawn_obstacle()
