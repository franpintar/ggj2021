extends Obstacle

class_name Asteroid

func _on_Asteroid_area_entered(area):
	if area is Bullet:
		area.queue_free()
		.change_hp(1)
