extends Obstacle

class_name BrokenExhaust

func _on_BrokenExhaust_area_entered(area):
	if area is Bullet:
		area.queue_free()
		.change_hp(1)
